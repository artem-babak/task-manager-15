package ru.t1c.babak.tm.controller;

import ru.t1c.babak.tm.api.controller.ITaskController;
import ru.t1c.babak.tm.api.service.ITaskService;
import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Task;
import ru.t1c.babak.tm.util.DateUtil;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Render all tasks in default order.
     */
    private void renderAllTasks() {
        final List<Task> tasks = taskService.findAll();
        renderTasks(tasks);
    }

    private void renderTasks(final List<Task> tasks) {
        for (int i = 0; i < tasks.size(); ) {
            final Task task = tasks.get(i++);
            if (task == null) continue;
            System.out.println("\t" + i + ". " + task);
        }
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        System.out.println("\tENTER SORT:");
        System.out.println("\t" + Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public void showTaskListByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("\tENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        System.out.println("\tENTER BEGIN DATE");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("\tENTER END DATE");
        final Date dateEnd = TerminalUtil.nextDate();
        taskService.create(name, description, dateBegin, dateEnd);
    }

    private void showTask(final Task task) {
        System.out.println("\tID: " + task.getId());
        System.out.println("\tNAME: " + task.getName());
        System.out.println("\tDESCRIPTION: " + task.getDescription());
        final Status status = task.getStatus();
        if (status != null) System.out.println("\tSTATUS: " + status.getDisplayName());
        System.out.println("\tCREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("\tDATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("\tENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.findOneById(id);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        taskService.updateOneById(id, name, description);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.findOneByIndex(index);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        taskService.updateOneByIndex(index, name, description);
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.findOneById(id);
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.findOneByIndex(index);
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusByIndex(index, status);
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
    }

}
